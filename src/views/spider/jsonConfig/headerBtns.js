export default [{
  tooltip: '保存（Ctrl+S）',
  icon: 'save'
}, {
  tooltip: '测试（Ctrl+Q）',
  icon: 'caret-right'
}, {
  tooltip: '撤销（Ctrl+Z）',
  icon: 'redo'
}, {
  tooltip: '反撤销（Ctrl+Y）',
  icon: 'undo'
}, {
  tooltip: '历史版本',
  icon: 'history'
}, {
  tooltip: '全选（Ctrl+A）',
  icon: 'check-square'
}, {
  tooltip: '剪切（Ctrl+X）',
  icon: 'scissor'
}, {
  tooltip: '复制（Ctrl+C）',
  icon: 'copy'
}, {
  tooltip: '粘贴（Ctrl+V）',
  icon: 'snippets'
}, {
  tooltip: '删除（Delete）',
  icon: 'delete',
  click: 'handleDelSelectCells'
}, {
  tooltip: '编辑 XML',
  icon: 'code'
}, {
  tooltip: '打印 XML',
  icon: 'printer'
}, {
  tooltip: '调试（Ctrl+Q）',
  icon: 'bug'
}, {
  tooltip: '下一步',
  icon: 'step-forward'
}, {
  tooltip: '停止',
  icon: 'stop'
}]